from wtforms import Form, StringField, TextField, validators, HiddenField


# Create your forms

class PetForm(Form):
    name = StringField('Nombre', [validators.Required(message = 'Nombre requerido.'),
                                validators.Length(min = 3, max = 25, message = 'Ingrese un nombre valido.'),
                                ])
    animal = StringField('Animal', [validators.Required(message = 'Animal requerida.'), ])
    age = StringField('Años', [validators.Required(message = 'Años requerido.'), ])
    owner = StringField('Dueño', [validators.Required(message = 'Dueño requerido.'), ])
    honeypot = HiddenField('', [validators.Length(min = 0, max = 0)])

class OwnerForm(Form):
    owner = StringField('Dueño', [validators.Required(message = 'Dueño requerido.')])
