from base64 import b64encode

from flask import Flask, flash, redirect, render_template
from flask import request, url_for, make_response, session
from flask_wtf.csrf import CSRFProtect

from forms.forms import PetForm, OwnerForm
from models.models import db, Pet
from config import DevelopmentConfig

app = Flask(__name__)
app.config.from_object(DevelopmentConfig())
csrf = CSRFProtect(app)


@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
def index():
    form_pet = PetForm(request.form)
    form_owner = OwnerForm(request.form)
    if  request.method == 'POST' and form_pet.validate():
        image = request.files['image']
        pet = Pet(
                name = form_pet.name.data,
                animal = form_pet.animal.data,
                age = form_pet.age.data,
                owner = form_pet.owner.data,
                image_name = image.filename,
                data_image = image.read()
                )
        owner = form_pet.owner.data
        db.session.add(pet)
        db.session.commit()
        flash('Gracias por registrar tu mascota.')
        return redirect(url_for('registered', owner = owner))

    elif request.method == 'POST' and form_owner.validate():
        owner = form_owner.owner.data
        return redirect(url_for('query', owner = owner))
    return render_template('index.html', form_pet = form_pet, form_owner = form_owner)


@app.route('/query/<owner>')
def query(owner):
    try:
        query = Pet.query.filter(Pet.owner == owner).first()
        image = b64encode(query.data_image).decode('ascii')
        return render_template('query.html', result = query, image = image, owner = None)
    except Exception as e:
        return render_template('query.html', result = None, image = None, owner = owner)


@app.route('/registered/<owner>')
def registered(owner):
    return render_template('registered.html', owner = owner)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == '__main__':
    db.init_app(app)
    with app.app_context():
        db.create_all()

    app.run()
