import os

# Configuration handling

class BaseConfig():
    DEBUG = False
    SECRET_KEY = 'est0-es_una-clav3-criptografic@'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WTF_CSRF_ENABLED = False

class ProductionConfig(BaseConfig):
    DATABASE_URI = 'mysql://user@localhost/foo'

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    DATABASE =  os.getcwd() + '/db.sqlite3'
    SQLALCHEMY_DATABASE_URI = 'sqlite:////' + DATABASE

class TestingConfig(BaseConfig):
    TESTING = True
