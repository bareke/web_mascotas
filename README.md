## Aplicación Web Veterinaria

Aplicación web desarrollada con [Flask](http://flask.pocoo.org/) micro-framework de Python.

---

## Funcionalidades del aplicativo

- Registra mascotas con sus datos e imagen.
- Consulta una mascota con el nombre del dueño registrado.

---

## Pasos necesarios para iniciar la aplicación

1. Se recomienda utilizar **Python 3.4** o superior pero también es compatible con **Python 2.7**.
2. Instalar los paquetes contenidos en **requirements.txt** utilizando el comando
``` pip install -r requirements.txt
```
3. Ejecutar el archivo pet.py
4. Abrir el navegador e ir a la dirección http://localhost:5000/
